#!/bin/sh

#Python, in its infinite shittiness, doesn't check https certificates. 

curl -s "https://csclub.uwaterloo.ca" > /dev/null || (echo "FAIL on csc" && exit 2)
curl -s "https://webauth.csclub.uwaterloo.ca" > /dev/null || (echo "FAIL on webauth" && exit 3)
curl -s "https://mail.csclub.uwaterloo.ca" > /dev/null || (echo "FAIL on mail" && exit 4)

exit 0
