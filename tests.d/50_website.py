#!/usr/bin/env python
import urllib2, sys

pages=["https://csclub.uwaterloo.ca",
       "http://mirror.csclub.uwaterloo.ca",
       "http://git.csclub.uwaterloo.ca"]

for theurl in pages:
  try:
    r = urllib2.urlopen(theurl, timeout=3)
    page = r.read()
    if not len(page) > 1000:
      print "FAIL, didn't have enough data"
      sys.exit(2)
    if not ("Computer Science Club" in page or "git.csclub.uwaterloo.ca Git" in page):
      print "FAIL,", theurl, "doesn't mention Computer Science Club"
      sys.exit(3)
  except urllib2.URLError as e:
    print "FAIL, exception ", e
    sys.exit(1)

# So read() didn't throw an exception, nor did opening,
# and we have enough data, so likely good.
sys.exit(0)
