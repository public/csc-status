#!/usr/bin/env python

import sys, socket

services = [
  ('caffeine.csclub.uwaterloo.ca',            [22, 80, 443]),
  ('mail.csclub.uwaterloo.ca',                [25, 80, 143, 465, 587, 993]),
  ('artificial-flavours.csclub.uwaterloo.ca', [22, 389, 636]),
  ('taurine.csclub.uwaterloo.ca',             [21, 22, 53, 80, 81, 443, 8000, 8080]),
  ('mirror.csclub.uwaterloo.ca',              [21, 22, 80, 873]),
 # ('denardo.csclub.uwaterloo.ca',             [22]),
  ('corn-syrup.csclub.uwaterloo.ca',          [22]),
  ('ascorbic-acid.csclub.uwaterloo.ca',       [21, 22, 53, 80, 81, 443, 8000, 8080]), #all ssh
  ('ginseng.csclub.uwaterloo.ca',             [22, 111, 389, 464, 636, 749, 888, 2049]),
  ('webauth.csclub.uwaterloo.ca',             [22, 443]),
  ('wiki.csclub.uwaterloo.ca',                [80, 443]),
]

for S in services:
  try:
   host = socket.gethostbyname(S[0])
   for port in S[1]:
     s = socket.socket()
     s.settimeout(3)
     s.connect( (host , port) )
     s.close()
  except socket.error, e:
   print "FAIL socket connect to ", S[0], host, port
   sys.exit(1)
