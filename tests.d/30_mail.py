#!/usr/bin/env python
import socket, sys


for S in ['mail.csclub.uwaterloo.ca', 'mail2.csclub.uwaterloo.ca']:
  s = socket.socket()
  try:
    s.connect((S, 25))
    l220 = s.recv(128)
  except socket.error, e:
    print "FAIL", S, e
    sys.exit(1)

  if not (l220.startswith("220 ") and "csclub.uwaterloo.ca" in l220):
    print "FAIL: Unexpected mail server start", S, l220
    sys.exit(1)
  s.send("EHLO localhost\n")
  caps = s.recv(1024).split('\n')
  if len([x for x in caps if not (x.startswith('250') or len(x) == 0)]) != 0:
    print "FAIL: Unexpected reply to EHLO", S
    sys.exit(2)

sys.exit(0)
